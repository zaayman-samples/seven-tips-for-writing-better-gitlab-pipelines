# Seven tips for writing better GitLab pipelines

Read the full blog [here](https://medium.com/@jeffreyzaayman/seven-tips-for-writing-better-gitlab-pipelines-c94f348ad0b9).

1. Exclude inherited behaviour with `null`
2. Extend multiple templates for more granular behaviour
3. Use `!reference` to reintroduce behaviour from extended templates or jobs
4. Use `!reference` to extend a template without actually extending it
5. Use `needs` to start a job in the next stage before the current stage finishes
6. Use YAML anchors and aliases
7. How to deal with monorepos
